using UnityEngine;
using Leap;

//Extension to the unity vector class. Transform coord space from Leap to Unity
public static class UnityVectorExtension
{	
	//For Directions
	public static Vector3 ToUnity(this Vector lv)
	{
		return new Vector3( lv.x, lv.y, -lv.z );
	}

	//Copy prevent
	public static void ToUnity(this Vector lv, ref Vector3 to)
	{
		to.Set(lv.x, lv.y, -lv.z);
	}

	public static void ToUnityNormal(this Vector lv, ref Vector3 to)
	{
		to.Set(-lv.x, -lv.y, lv.z);
	}
}

