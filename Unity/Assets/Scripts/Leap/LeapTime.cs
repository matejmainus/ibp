using UnityEngine;
using System.Collections;

/* Should be used as deltaTime for Leap classes.
 * If Time.timeScale is set to 0, then LeapAPI will not work
 */
public class LeapTime
{
	private static float m_oldTime = 0f;

	public static float deltaTime {get; private set;}

	public static void UpdateTime()
	{
		if(m_oldTime < Time.timeSinceLevelLoad)
		{
			deltaTime = Time.timeSinceLevelLoad - m_oldTime;
			m_oldTime = Time.timeSinceLevelLoad;
		}
	}
}

