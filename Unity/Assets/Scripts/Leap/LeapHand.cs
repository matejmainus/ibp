using System;
using UnityEngine;
using Leap;

public class LeapHand {

	const float INVALID_TIME = 1.0f;
	const float INACTIVE_TIME = 0.4f;
	const float READY_TIME = 0.5f;
	const float STABLE_TIME = 0.4f;

	//Position sqr diff
	const float READY_POS_OFFSET = 2f*2f;
	const float SAME_POS_OFFSET = 20f*20f;

	//Normal angle diff
	const float READY_NORM_ANGLE = 3f;
	const float STABLE_NORM_ANGLE = 20f;
	const float SAME_NORM_ANGLE = 15f;

	//Direciton angle diff
	const float READY_DIR_ANGLE = 3f;
	const float STABLE_DIR_ANGLE = 20f;
	const float SAME_DIR_ANGLE = 15f;

	const int READY_FINGERS_COUNT = 2;

	private float m_stableTime;
	private float m_invalidTime;
	private float m_readyTime;
	
	private LeapFistGesture m_fistGesture;

	private Hand m_hand;
	private Hand m_oldHand;

	private Vector3 m_posOffset;
	private Vector3 m_normalOffset;
	private Vector3 m_directionOffset;
	
	public Vector3 Position {get; private set;}
	public Vector3 PositionNormalized {get; private set;}
	public Vector3 Normal {get; private set;}
	public Vector3 NormalNormalized {get; private set;}
	public Vector3 Direction {get; private set;}
	public Vector3 DirectionNormalized {get; private set;}

	public int Id {
		get {
			return m_hand.Id;
		}
	}

	public FingerList Fingers {
		get {
			return m_hand.Fingers;
		}
	}

	// Critical state of hand, beacuse it going to be removed soon.
	public bool IsValid {
		get {
			return m_invalidTime <= INVALID_TIME;
		}
	}

	// Hand become inactive when is not recognised for a while
	public bool IsActive {
		get {
			return m_invalidTime <= INACTIVE_TIME && IsReady;
		}
	}

	// Tels that hand is realy hand, due to some bouncy hidden objects
	public bool IsReady {
		get {
			return m_readyTime >= READY_TIME;
		}
	}

	// Is hand stable or is in wrong orientation when Leap can not recognize it
	// and provides random data
	public bool IsStable {
		get {
			return m_stableTime > STABLE_TIME;
		}
	}

	public LeapFistGesture FistGesture {get; private set;}

	public delegate void HandChangedHandler(LeapHand hand);
	
	public event HandChangedHandler HandChangedEvent;

	public LeapHand(Hand h)
	{
		m_invalidTime = 0f;
		m_readyTime = 0f;
		m_stableTime = 0f;

		m_oldHand = Hand.Invalid;
		m_hand = h;

		Position = Vector3.zero;
		Direction = Vector3.forward;
		Normal = Vector3.down;

		FistGesture = new LeapFistGesture(this);
	}

	public void UpdateHand(Hand h)
	{
		if(h.IsValid)
		{
			m_invalidTime = 0f;

			m_oldHand = m_hand;
			m_hand = h;

			Vector3 lastPos = m_oldHand.PalmPosition.ToUnity();
			Vector3 lastNorm = m_oldHand.PalmNormal.ToUnity();
			Vector3 lastDir = m_oldHand.Direction.ToUnity();

			Vector3 pos = h.PalmPosition.ToUnity();
			Vector3 norm = h.PalmNormal.ToUnity();
			Vector3 dir = h.Direction.ToUnity();
		
			if(IsReady) {

				/* Hands become unstable when some rapid rotation changed started, 
				 * then m_stable time set to Frame Delta time, no set a position, rot, dir
				 * and it waits for next frame.
				 * If in new frame was rapid change of orientation detected again, it repeat.
				 * If not, wait for while (add Frame delta time to m_stable time) whether its ok
				 * and then set position.
				 */
				if(Vector3.Angle(norm, lastNorm) < STABLE_NORM_ANGLE &&
						Vector3.Angle(dir, lastDir) < STABLE_DIR_ANGLE)
				{
					if(IsStable)
					{
						Position = pos;
						PositionNormalized = pos - m_posOffset;
						Direction = dir;
						DirectionNormalized = dir - m_directionOffset;
						Normal = norm;
						NormalNormalized = norm - m_normalOffset;
					}
					else
					{
						m_stableTime += LeapTime.deltaTime;
					}
				}
				else
				{//Hand is unstable
					m_stableTime = LeapTime.deltaTime; 
				}

				HandChangedEvent(this);
			}
			else {
				//Hand is not ready so wait until its not stable
				if(Vector3.SqrMagnitude(lastPos - pos) < READY_POS_OFFSET &&
					Vector3.Angle(norm, lastNorm) < READY_NORM_ANGLE &&
					Vector3.Angle(dir, lastDir) < READY_DIR_ANGLE &&
				   h.Fingers.Count >= READY_FINGERS_COUNT)
				{
					m_readyTime += LeapTime.deltaTime;

					if(IsReady)
					{
						SetOffsets(pos, norm, dir);
						m_stableTime = STABLE_TIME;
					}
				}
				else
				{
					m_readyTime = 0f;
				}
			} 
		}
		else
			m_invalidTime += LeapTime.deltaTime;
	}
	
	void SetOffsets(Vector3 pos, Vector3 norm, Vector3 dir)
	{
		m_posOffset = pos;
		m_normalOffset = norm - Vector3.down;
		m_directionOffset = dir - Vector3.forward;
	}

	public bool IsSameHand(Hand h)
	{
		Vector3 pos = (h.PalmPosition - m_hand.PalmPosition).ToUnity();
		Vector3 norm = h.PalmNormal.ToUnity();
		Vector3 dir = h.Direction.ToUnity();

		return Vector3.SqrMagnitude(pos) < SAME_POS_OFFSET &&
			Vector3.Angle(Normal, norm) < SAME_NORM_ANGLE &&
			Vector3.Angle(Direction, dir) < SAME_DIR_ANGLE;
	}
}
