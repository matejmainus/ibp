using System.Collections;

public class LeapHandFollow
{
	public LeapHand Hand{get; private set;}
	public bool NewHand {get; private set;}

	private LeapController m_controller;

	public LeapHandFollow(LeapController controller)
	{
		m_controller = controller;

		m_controller.HandsChangedEvent += new LeapController.HandsChangedHandler(Update);
	}

	void Update()
	{
		if(!(Hand != null && Hand.IsActive))
		{
			Hand = m_controller.GetActiveHand();
			NewHand = true;
		}
		else
			NewHand = false;
	}
}

