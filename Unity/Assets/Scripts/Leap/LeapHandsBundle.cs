using UnityEngine;
using System.Collections.Generic;

/* Folow one or more hand with same output data (pos, normal, direction)
 */
public class LeapHandsBundle 
{
	const float READY_TIME = 0.4f;
	const int MAX_HANDS = 2;

	// If one hand detected wait for other one
	public bool IsReady {
		get
		{
			return m_readyTime >= READY_TIME;
		}
	}

	// If its all hands in fist gesture
	public bool IsActive {
		get
		{
			return m_active && IsReady;
		}
	}
	
	public Vector3 Position {get; private set;}
	public Vector3 Normal {get; private set;}
	public Vector3 Direction {get; private set;}
	
	public List<LeapHand> Hands {get; private set;}
	public int MaxHands {get; private set;}

	private LeapController m_controller;

	private bool m_active;
	private float m_readyTime;
	
	public LeapHandsBundle(LeapController controller)
	{
		m_active = false;
		m_readyTime = 0f;
		m_controller = controller;

		m_controller.HandsChangedEvent += new LeapController.HandsChangedHandler(Update);

		MaxHands = 0;
		Hands = new List<LeapHand>(MAX_HANDS);
	}

	void Update()
	{
		RemoveInactiveHands();

		if(IsFree())
			AddReadyHands();

		if(IsReady)
		{
			//Set max hands count
			if(MaxHands == 0)
				MaxHands = Hands.Count;
			
			SetTransform();
			SetActive();
		}
		else
		{
			if(Hands.Count > 0 && IsHandsReady())
			{
				m_readyTime += LeapTime.deltaTime;
			}
			else
				m_readyTime = 0f;
		}
	}

	public bool IsFree()
	{
		bool free = Hands.Count < MaxHands || MaxHands == 0;

		for(int i = 0 ; i < Hands.Count ; i++)
		{
			free = free || !Hands[i].IsValid;
		}

		return free;
	}

	public LeapHand Leftmost()
	{
		LeapHand leftmost = null;
		LeapHand hand;
		for(int i = 0 ; i < Hands.Count ; i++)
		{
			hand = Hands[i];

			if(leftmost == null || leftmost.Position.x > hand.Position.x)
				leftmost = hand;
		}
		
		return leftmost;
	}
	
	public LeapHand Rightmost()
	{
		LeapHand rightmost = null;
		
		LeapHand hand;
		for(int i = 0 ; i < Hands.Count ; i++)
		{
			hand = Hands[i];

			if(rightmost == null || rightmost.Position.x < hand.Position.x)
				rightmost = hand;
		}
		
		return rightmost;
	}

	public void Clear()
	{
		LeapHand hand;
		for(int i = Hands.Count - 1 ; i >= 0 ; i--)
		{
			hand = Hands[i];
			RemoveHand(hand);
		}
	}

	void RemoveInactiveHands()
	{
		LeapHand hand;
		for(int i = Hands.Count - 1 ; i >= 0 ; i--)
		{
			hand = Hands[i];
			if(!hand.IsValid)
				RemoveHand(hand);
		}
	}

	void AddReadyHands()
	{
		LeapHand hand;
		for(int i = 0; IsFree() && i < m_controller.Hands.Count ; i++)
		{
			hand = m_controller.Hands.Values[i];
			
			if(hand.IsReady)
				AddHand(hand);
		}
	}

	bool AddHand(LeapHand h)
	{
		if(!IsFree() || Hands.Contains(h))
			return false;
		
		Hands.Add(h);
		
		m_readyTime = 0f;
		
		return true;
	}
	
	void RemoveHand(LeapHand h)
	{
		Hands.Remove(h);
		
		if(Hands.Count == 0) {
			MaxHands = 0;
			m_readyTime = 0f;
		}
	}

	bool IsHandsReady()
	{
		for(int i = 0 ; i < Hands.Count ; i++)
		{
			if(!Hands[i].IsReady)
				return false;
		}

		return true;
	}

	void SetActive()
	{
		bool bundleFist = true;
		
		for(int i = 0 ; i < Hands.Count ; i++)
		{
			if(!Hands[i].FistGesture.IsActive)
			{
				bundleFist = false;
				break;
			}
		}
		
		if(bundleFist == m_active)
		{
			m_active = !m_active;
		}
	}

	void SetTransform()
	{
		if(MaxHands == 1)
		{
			if(Hands.Count == 1)
				TransformOneHand();
		}
		else if(MaxHands == 2)
		{
			if(Hands.Count == 2)
				TransformTwoHands();
		}
	}

	void TransformOneHand()
	{
		LeapHand h = Hands[0];

		Vector3 norm = h.NormalNormalized;
		norm = -norm;

		Position = h.Position;
		Normal = norm;
		Direction = h.DirectionNormalized;
	}

	void TransformTwoHands()
	{
		LeapHand h1 = Leftmost();
		LeapHand h2 = Rightmost();

		// Direction is arithmetic average of direction from both hands
		Vector3 dir = (h1.DirectionNormalized + h2.DirectionNormalized)/2;
	
		/* Average of normals should point from left to right due to
		* cross product from normal and direction. So hands normals have to be mirrored
		* around Z-axies to point as same as direction of normal calculated from Y-position diff
		 */
		Vector3 h1norm = h1.NormalNormalized;
		h1norm.x = -h1norm.x;

		Vector3 h2norm = h2.NormalNormalized;
		h2norm.x = -h2norm.x;

		/* Weighted average with empashis for position diff, then to right hand rotation
		 * and than to left hand rotation
		 */
		Vector3 handsNorm = (1.5f*(h1.Position - h2.Position) +
		                     h1norm + 
		                     1.2f*h2norm) / 3.7f;

		handsNorm.z = 0;
	
		Vector3 handsDir = dir;
		handsDir.x = 0;

		Position = (h1.Position + h2.Position)/2;

		//Calc cross product of normal and direction for normal vector pointing up
		Normal = Vector3.Cross(handsNorm, handsDir).normalized;
		Direction = dir;
	}
}
