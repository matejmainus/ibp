using System;
using UnityEngine;

public class LeapFistGesture
{
	private const int FIST_FINGER_COUNT = 1;
	private const float GESTURE_RECOG_TIME = .4f;
	private const float GESTURE_FAIL_TIME = 0.1f;
	private const float MAX_ANGLE = 15f;

	public bool IsActive {get; private set;}
	
	private float m_gestureTime;

	public LeapFistGesture (LeapHand hand)
	{
		hand.HandChangedEvent += new LeapHand.HandChangedHandler(Update);
	}

	void Update(LeapHand h)
	{

		/* If hand is not in horizontal position, leap fingers detecture fails almost time
		 * so for precise detection, hand have to be in horizontal position*/
		if(Vector3.Angle(h.NormalNormalized, Vector3.down) < MAX_ANGLE)
		{
			if(h.Fingers.Count <= FIST_FINGER_COUNT)
			{
				if(m_gestureTime < GESTURE_RECOG_TIME)
					m_gestureTime += LeapTime.deltaTime;
				else
					IsActive = true;
			}
			else
			{
				if(m_gestureTime > 0f)
					m_gestureTime -= 1.2f * LeapTime.deltaTime;

				IsActive = m_gestureTime > GESTURE_FAIL_TIME;
			}
		}
	}
};