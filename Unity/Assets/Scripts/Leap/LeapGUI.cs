using UnityEngine;
using System.Collections;

public class LeapGUI : MonoBehaviour
{
	public float MaxX = 150f;
	public float MaxY = 150f;
	public float YOffset = 50f;

	public Texture2D FocusTexture;

	public Texture2D CursorTexture;
	public Color NormalCursor;
	public Color FocusCursor;
	public Color ClickCursor;

	public float ActiveTimer = 1.0f;

	private LeapHandFollow m_followHand;
	
	//private Vector2 m_lastPoint;
	private Vector2 m_point;

	private float m_focusTimer;
	private bool m_onElement;

	private bool IsOnElement {
		get{ return m_focusTimer > 0.01;}
	}

	private bool IsActive {
		get { return m_focusTimer > ActiveTimer;}
	}

	public LeapGUI()
	{
		m_point = Vector2.zero;
		//m_lastPoint = Vector2.zero;
		m_focusTimer = 0f;
	}

	void Awake()
	{
		LeapController leapController = this.GetComponent<LeapController>();
		m_followHand = new LeapHandFollow(leapController);
	}
	
	void OnGUI()
	{
		GUI.depth = 0;

		SetPoint();

		//Bugfix for GetLastRect();
		if (Event.current.type == EventType.Repaint)
			SetTimer();

		if (m_point != Vector2.zero)
			DrawPointer();
	}

	void SetPoint()
	{
		if(m_followHand.Hand != null)
		{
			m_point = HandToScreen(m_followHand.Hand);
		}
		else
			m_point = Vector2.zero;
	}

	void SetTimer()
	{
		if(m_point != Vector2.zero)
		{
			if(m_onElement)
				m_focusTimer += LeapTime.deltaTime;
			else
				m_focusTimer = 0f;
		}
		else
			m_focusTimer = 0f;

		m_onElement = false;
	}
	
	void DrawPointer()
	{
		Rect rect = new Rect(m_point.x - CursorTexture.width/2,
		                     m_point.y - CursorTexture.height/2,
		                     CursorTexture.width, 
		                     CursorTexture.height);

		Color oldColor = GUI.color;

		if(IsOnElement)
			GUI.color = Color.Lerp(FocusCursor, ClickCursor, m_focusTimer / ActiveTimer);
		else
			GUI.color = NormalCursor;

		GUI.DrawTexture(rect, CursorTexture);

		GUI.color = oldColor;
	}

	public bool LayoutButton(Rect container)
	{
		if (Event.current.type == EventType.Repaint)
		{
			return Button(GUILayoutUtility.GetLastRect(), container);
		}
		else
			return false;
	}

	public bool Button(Rect rect)
	{
		return Button(rect, new Rect());
	}

	public bool Button(Rect rect, Rect container)
	{
		Rect abs = AbsoluteRect(rect, container);

		if(abs.Contains(m_point))
		{
			GUI.DrawTexture(rect, FocusTexture);

			m_onElement = true;

			if(IsActive)
			{
				m_focusTimer = 0f;
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}

	public float LayoutSlider(Rect container, float val, float min, float max)
	{
		if (Event.current.type == EventType.Repaint)
		{
			return Slider(GUILayoutUtility.GetLastRect(), val, min, max, container);
		}

		return val;
	}

	public float Slider(Rect rect, float val, float min, float max)
	{
		return Slider(rect, val, min, max, new Rect());
	}

	public float Slider(Rect rect, float val, float min, float max, Rect container)
	{
		Rect abs = AbsoluteRect(rect, container);

		if(abs.Contains(m_point))
		{
			float pointVal = GetSliderValue(abs, m_point, min, max);

			if(IsActive)
			{
				m_onElement = true;

				GUI.DrawTexture(rect, FocusTexture);

				return pointVal;
			}
			else
			{
				m_onElement = Mathf.Abs(val - pointVal) < 0.05f;
			}
		}

		return val;
	}

	private float GetSliderValue(Rect rect, Vector2 point, float min, float max)
	{
		float range = max - min;
		float x = point.x - rect.x;

		return (range * x) / rect.width + min;
	}

	private Rect AbsoluteRect(Rect rect, Rect container)
	{
		rect.x += container.x;
		rect.y += container.y;
	
		return rect;
	}

	private Vector2 HandToScreen(LeapHand h)
	{
		return new Vector2(((h.Position.x + MaxX) * Screen.width / (2*MaxX)),
		                   Screen.height - ((h.Position.y - YOffset) * Screen.height / MaxY));
	}
}