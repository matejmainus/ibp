﻿using UnityEngine;
using System.Text;

public class LeapDebugOverlay : MonoBehaviour {

	public GameObject LeapController;

	void OnGUI()
	{
		StringBuilder output = new StringBuilder();
		output.AppendFormat("{0:0.00}\n", LeapTime.deltaTime);

		LeapController leapController = LeapController.GetComponent<LeapController>();
		if(leapController)
			AppendController(leapController, output);

		Rect box = new Rect(Screen.width - 210, 10, 200, 200);

		GUI.backgroundColor = Color.gray;

		GUI.Box(box,output.ToString());
	}

	private void AppendController(LeapController leapController, StringBuilder output)
	{
		if(leapController.IsLeapConnected)
		{
			if(leapController.Hands.Count > 0)
			{
				LeapHand h;
				for(int i = 0; i < leapController.Hands.Count ; i++)
				{
					h = leapController.Hands[i];
					output.AppendFormat("{0}: {1:Stable;0;Unstable} {2:Active;0;InActive} {3:Ready;0;NotReady}\n", 
					                    h.Id, h.IsStable.GetHashCode(), h.IsActive.GetHashCode(), h.IsReady.GetHashCode());
				}
			}
			else
				output.AppendLine("No hand detected");
		}
		else
			output.Append("No Leap");
	}
}
