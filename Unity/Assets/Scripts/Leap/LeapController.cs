using UnityEngine;
using System.Collections.Generic;
using Leap;
using System;

public class LeapController : MonoBehaviour{

	public delegate void HandsChangedHandler();

	public event HandsChangedHandler HandsChangedEvent;

	public SortedList<int, LeapHand> Hands {get; private set;}

	private Leap.Controller m_controller;
	private Leap.Frame m_frame;

	public bool IsLeapConnected
	{
		get{
			return m_controller.IsConnected;
		}
	}

	public LeapController() 
	{
		Hands = new SortedList<int,LeapHand>();

		m_frame = null;
		m_controller = new Leap.Controller();
	}
	
	void Update ()
	{
		Leap.Frame lastFrame = m_frame == null ? Frame.Invalid : m_frame;
		m_frame = m_controller.Frame();

		if(m_frame.Id > lastFrame.Id)
		{
			LeapTime.UpdateTime();

			UpdateHands(m_frame, lastFrame);
		}

		HandsChangedEvent();
	}
	
	public LeapHand GetActiveHand()
	{
		LeapHand hand;
		for(int i = 0 ; i < Hands.Count ; i++)
		{
			hand = Hands.Values[i];
			if(hand.IsActive)
				return hand;
		}

		return null;
	}

	void UpdateHands(Leap.Frame newFrame, Leap.Frame oldFrame)
	{
		//Update or remove founded hands
		LeapHand hand;
		for(int i = Hands.Count - 1 ; i >= 0 ; i--)
		{
			hand = Hands.Values[i];
			hand.UpdateHand(newFrame.Hand(hand.Id));

			if(!hand.IsValid)
			{
				Hands.Remove(hand.Id);
			}
		}

		Leap.Hand lhand;
		//New and alive hands detection
		for(int i = 0 ; i < newFrame.Hands.Count ; i++)
		{
			lhand = newFrame.Hands[i];
			if(!Hands.ContainsKey(lhand.Id))
				HandFound(lhand);
		}
	}

	void HandFound(Hand h)
	{
		/*
		If new founded hand (has different id)
		has similar values as old one, 
		change hand id to new one, and keep it calm.
		*/
		LeapHand lHand;
		for(int i = Hands.Count - 1 ; i >= 0 ; i--)
		{
			lHand = Hands.Values[i];
			if((lHand.IsActive) && lHand.IsSameHand(h)) {
				lHand.UpdateHand(h);
				Hands.RemoveAt(i);
				Hands.Add(lHand.Id, lHand);
				return;
			}
		}

		//Hand is not as "same" as previously founded hand
		LeapHand hand = new LeapHand(h);
		Hands.Add(h.Id, hand);
	}
}
