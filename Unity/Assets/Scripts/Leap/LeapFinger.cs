﻿using System.Collections;
using Leap;

public class LeapFinger
{
	public Finger Finger {get; private set;}

	public LeapFinger(Finger f)
	{
		UpdateFinger(f);
	}

	public void UpdateFinger(Finger f)
	{
		Finger = f;
	}
}
