﻿using UnityEngine;
using System.Collections.Generic;
using Leap;

public class LabController : MonoBehaviour {

	public float MaxAngle = 40f;

	public LeapHandsBundle HandsBundle {get; private set;}

	protected GameController m_gameController;

	void Awake ()
	{	
		m_gameController = GameObject.FindGameObjectWithTag(Tags.GameController)
			.GetComponent<GameController>();
	
		LeapController leapController = GameObject.FindGameObjectWithTag(Tags.LeapController)
			.GetComponent<LeapController>();

		HandsBundle = new LeapHandsBundle(leapController);
	}

	protected virtual void Rotate()
	{
		Vector3 norm = Vector3.Lerp(
			Vector3.up, 
			HandsBundle.Normal, 
			Settings.Sensitivity);

		if(Vector3.Angle(norm, Vector3.up) < MaxAngle)
		{
			transform.rotation = Quaternion.FromToRotation(Vector3.up, norm);
		}
	}
}
