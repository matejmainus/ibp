﻿using UnityEngine;
using System.Collections;

public class HoleController : MonoBehaviour {

	public bool Heaven = false;
	
	private BallsManager m_ballsMgr;
	private Vector3 m_endPoint;

	void Awake()
	{
		m_ballsMgr = GameObject.FindGameObjectWithTag(Tags.GameController)
			.GetComponent<BallsManager>();

		m_endPoint = transform.position;
		m_endPoint.y -= 0.1f;
	}

	void OnTriggerEnter(Collider other)
	{
		GameObject go = other.gameObject;
		
		if(go.tag == Tags.Ball)
		{
			go.rigidbody.isKinematic = true;
		}
	}

	void OnTriggerStay(Collider other)
	{
		GameObject go = other.gameObject;
		
		if(go.tag == Tags.Ball)
		{
			Vector3 dir = transform.position - go.transform.position;

			go.transform.position = Vector3.Lerp(go.transform.position, 
			                                     m_endPoint + dir, 
			                                     1.5f * Time.deltaTime);
		}
	}

	void OnTriggerExit(Collider other)
	{
		GameObject go = other.gameObject;
		
		if(go.tag == Tags.Ball)
		{
			go.rigidbody.isKinematic = false;
			m_ballsMgr.BallInHole(go, Heaven);
		}
	}
}
