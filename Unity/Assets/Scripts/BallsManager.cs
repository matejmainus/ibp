﻿using UnityEngine;
using System.Collections.Generic;

public class BallsManager : MonoBehaviour {

	private const int MAX_BALL_COUNT = 20;

	public GameObject BallPrefab;
	public float PutBallDelay = .3f;
	public float RandomPositionOffset = .01f;

	public List<GameObject> Balls {get; private set;}

	GameController m_gameController;
	LevelsManager m_levelsMgr;

	Queue<GameObject> m_queue;
	float m_timer;

	public BallsManager()
	{
		m_queue = new Queue<GameObject>(MAX_BALL_COUNT);

		//set timer as PutBallDealy to put first ball immediately
		m_timer = PutBallDelay;
		
		Balls = new List<GameObject>(MAX_BALL_COUNT);
	}

	void Awake ()
	{
		m_gameController = gameObject.GetComponent<GameController>();

		m_levelsMgr = GameObject.FindGameObjectWithTag(Tags.LevelsManager)
			.GetComponent<LevelsManager>();
	}

	void Start () 
	{
		CreateBalls();
	}
	
	void Update () 
	{
		if(m_timer < PutBallDelay)
		{
			m_timer += Time.deltaTime;
		} 
		else if(m_queue.Count > 0)
		{
			DequeueBall();
			m_timer = 0f;
		}
	}

	public void BallInHole(GameObject ball, bool heaven)
	{
		if(heaven && IsBallActive(ball))
		{
			DestroyBall(ball);
			m_gameController.BallInHeaven();

			if(Balls.Count > 0)
				ActivateRandomBall();
			else
				m_gameController.EndGame();
		}
		else
		{
			ResetBall(ball);
			m_gameController.BallInHell();		
		}
	}

	public void ResetBall(GameObject ball)
	{
		if(!m_queue.Contains(ball))
		{
			ball.SetActive(false);

			m_queue.Enqueue(ball);
		}
	}

	public bool IsBallActive(GameObject ball)
	{
		return ball.GetComponent<BallController>().Active;
	}

	void DestroyBall(GameObject ball)
	{
		Balls.Remove(ball);
		Destroy(ball);
	}

	void ResetBallObj(GameObject ball)
	{
		//Entry points dont have be in object due to lazy loading of Labyrinth object
		GameObject[] entryPoints = GameObject.FindGameObjectsWithTag(Tags.EntryPoint);

		GameObject dropPoint = entryPoints[Random.Range(0, entryPoints.Length)];

		float offset = Random.Range(-RandomPositionOffset, RandomPositionOffset);
		
		ball.transform.position = dropPoint.transform.position + new Vector3(offset, offset, offset);
		ball.SetActive(true);

		BallController controller = ball.GetComponent<BallController>();
		controller.Reset();
	}

	void ActivateRandomBall()
	{
		LabyrinthModel lab = m_levelsMgr.Labyrinth;
		GameObject ball = null;

		do 
		{
			ball = Balls[Random.Range(0, Balls.Count)];
		}
		while(Balls.Count >= lab.ActiveBallCount && IsBallActive(ball));
		
		ActivateBall(ball);
	}

	void ActivateBall(GameObject ball)
	{
		BallController controller = ball.GetComponent<BallController>();
		controller.Activate();
	}

	void DequeueBall()
	{
		GameObject ball = m_queue.Dequeue();
		ResetBallObj(ball);
	}

	void CreateBalls()
	{
		GameObject ball;
		int createdActiveBalls = 0;

		LabyrinthModel lab = m_levelsMgr.Labyrinth;

		for(int i = 0 ; i < lab.BallCount ; i++)
		{
			ball = CreateBall();

			ball.SetActive(false);
			Balls.Add(ball);
			m_queue.Enqueue(ball);

			if(createdActiveBalls < lab.ActiveBallCount)
			{
				ActivateBall(ball);
				createdActiveBalls++;
			}
		}
	}

	GameObject CreateBall()
	{
		GameObject ball = Instantiate(BallPrefab) as GameObject;
		
		return ball;
	}
}
