﻿using UnityEngine;

public class LevelsManager : MonoBehaviour {

	public float DimmTimer = 2f;

	public int LabId = -1;

	public Texture2D LoadingTexture;
	public GUISkin Skin;

	public bool Loading {get; private set;}

	public LevelModel Level {get; private set;}
	public LabyrinthModel Labyrinth {get; private set;}

	private static LevelsManager m_instance;

	public LevelsManager()
	{
		Loading = false;
	}

	void Awake()
	{
		if(m_instance == null)
		{
			DontDestroyOnLoad(gameObject);
			m_instance = this;
		}
		else
		{
			Destroy (gameObject);
			return;
		}
		
		if(LabId > -1)
		{
			Labyrinths labyrinths = Labyrinths.Instance();
			Labyrinth = labyrinths.Find(lab => lab.Id == LabId);

			Levels levels = Levels.Instance();

			Level = levels.Find(level => level.LevelName == Application.loadedLevelName);
		}
	}
	
	void OnGUI()
	{
		GUI.skin = Skin;

		if(Loading)
		{
			GUI.depth = -1;

			GUI.DrawTexture(new Rect(0,0,Screen.width, Screen.height), LoadingTexture);
			GUI.Label(new Rect(Screen.width/5f, Screen.height / 2, 80, 30), "Loading ...");
		}
	}

	void OnLevelWasLoaded()
	{
		//IF menu is loaded, Level == null
		if(Labyrinth != null)
			InstantiateLabyrinth(Labyrinth);

		Loading = false;
	}

	public void LoadLevel(LabyrinthModel labyrinth)
	{
		LoadLevel(labyrinth, RandomLevel());
	}

	public void LoadLevel(LabyrinthModel labyrinth, LevelModel level)
	{
		Loading = true;

		Labyrinth = labyrinth;
		Level = level;

		Invoke("LoadLevel", DimmTimer);
	}

	public void LoadMainMenu()
	{
		Loading = true;

		Labyrinth = null;
		Level = null;

		Invoke("LoadMainMenuLevel", DimmTimer);
	}

	public void RestartLevel()
	{
		LoadLevel(Labyrinth, RandomLevel());
	}

	void LoadMainMenuLevel()
	{
		Application.LoadLevel("MainMenu");
	}

	void LoadLevel()
	{
		Application.LoadLevel(Level.LevelName);
	}

	void DestroyLabyrinth()
	{
		GameObject lab = GameObject.FindWithTag(Tags.Labyrinth);

		if(lab != null)
			Destroy(lab);
	}

	LevelModel RandomLevel()
	{
		Levels levels = Levels.Instance();

		return levels[Random.Range(0, levels.Count)];
	}

	GameObject InstantiateLabyrinth(LabyrinthModel labyrinth)
	{
		GameObject lab = Instantiate(Resources.Load("Labyrinths/"+labyrinth.ResourceName)) as GameObject;
		lab.tag = Tags.Labyrinth;
		lab.name = "Labyrinth";

		return lab;
	}
}
