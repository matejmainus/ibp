﻿using UnityEngine;
using System.Collections;

public class GameLabController : LabController {

	public float MoveSpeed = 0.1f;
	public float RotationSpeed = 0.2f;

	public Vector3 GamePosition;
	
	Vector3 m_startPosition;
	Quaternion m_startRotation;

	void Start () 
	{
		m_startPosition = transform.position;
		m_startRotation = transform.rotation;
	}

	void FixedUpdate () 
	{
		GameController.GameStateType gameState = m_gameController.GameState;

		if(gameState >= GameController.GameStateType.Ready && gameState < GameController.GameStateType.End)
		{
			if(HandsBundle.IsActive)
			{
				m_gameController.ResumeGame();
				
				Rotate ();
				Move(GamePosition);
			}
			else
			{
				m_gameController.PauseGame();

				LayDown();
			}
		}
		else
		{
			LayDown();
		}
	}

	void LayDown()
	{
		Move(m_startPosition);
		Rotate(m_startRotation);
	}

	void Move(Vector3 pos)
	{
		transform.position = Vector3.Lerp(transform.position, pos, Time.fixedDeltaTime * MoveSpeed);
	}

	void Rotate(Quaternion rot)
	{
		transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.fixedDeltaTime * RotationSpeed);
	}
}
