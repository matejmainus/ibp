﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GameStatsManager {

	private string FILE_PATH = Application.persistentDataPath + "/stats.dat";

	private Dictionary<int, List<GameStats>> m_stats;

	private static GameStatsManager m_instance = new GameStatsManager();

	public static GameStatsManager Instance()
	{
		return m_instance;
	}

	public GameStatsManager()
	{
		if(!TryLoad()) {
			m_stats = new Dictionary<int, List<GameStats>>();
		}
	}
	
	public void Save()
	{
		SortResults();

		using (FileStream fs = File.Create(FILE_PATH))
		{
			BinaryFormatter bf = new BinaryFormatter();
			bf.Serialize(fs, m_stats);

			fs.Flush();
		}
	}
	
	public bool TryLoad()
	{
		if(File.Exists(FILE_PATH))
		{
			using (FileStream fs = File.Open(FILE_PATH, FileMode.OpenOrCreate))
			{
				BinaryFormatter bf = new BinaryFormatter();
				m_stats = (Dictionary<int, List<GameStats>>) bf.Deserialize(fs);

				SortResults();
				//PrintResults();

				return true;
			}
		}
		else
			return false;
	}
	/*
	private void PrintResults()
	{
		string file ="C:/Users/Matej/data.txt";

		Debug.Log(file);

		using (FileStream fs = File.Open(file, FileMode.Create))
		{
			StreamWriter sw = new StreamWriter(fs);

			foreach(KeyValuePair<int, List<GameStats>> stats in m_stats)
			{
				foreach(GameStats stat in stats.Value)
				{
					StringBuilder sb = new StringBuilder();
					sb.Append(stats.Key);
					sb.Append(";");
					sb.Append(stat.Played.ToString("dd.MM.yyyy HH:mm"));
					sb.Append(";");
					sb.Append(stat.GameTime);
					sb.Append(";");
					sb.Append(stat.Score);
					sb.Append(";");

					sw.WriteLine(sb.ToString());
				}
			}

			sw.Flush();
		}
	}*/

	public List<GameStats> GetStats(LabyrinthModel lab)
	{
		List<GameStats> l = null;

		if(!m_stats.ContainsKey(lab.Id))
		{
			l = new List<GameStats>();
			m_stats.Add (lab.Id, l);
		}

		return m_stats[lab.Id];
	}

	public void AddStats(LabyrinthModel lab, GameStats stats)
	{
		List<GameStats> l = GetStats(lab);
	
		l.Add(stats);

		l.Sort();
	}

	private void SortResults()
	{
		foreach(List<GameStats> list in m_stats.Values)
		{
			list.Sort();
		}
	}
}
