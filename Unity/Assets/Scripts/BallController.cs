﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour {
	
	public float MinY = -4f;
	public Material ActiveMaterial;

	public bool Active {get; private set;}
	
	private BallsManager m_ballsMgr;

	public BallController()
	{
		Active = false;
	}

	void Awake()
	{
		m_ballsMgr = GameObject.FindGameObjectWithTag(Tags.GameController)
			.GetComponent<BallsManager>();
	}

	void Update () 
	{
		if(transform.position.y < MinY)
		{
			m_ballsMgr.ResetBall(gameObject);
		}
	}

	public void Reset()
	{
		rigidbody.velocity = Vector3.zero;
		rigidbody.angularVelocity = Vector3.zero;
	}

	public void Activate()
	{
		Active = true;

		renderer.material = ActiveMaterial;
	}
}
