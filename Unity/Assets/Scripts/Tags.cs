﻿using UnityEngine;
using System.Collections;

public class Tags
{
	public const string GameController = "GameController";
	public const string LevelsManager = "LevelsManager";
	public const string Ball = "Ball";
	public const string EntryPoint = "EntryPoint";
	public const string MainCamera = "MainCamera";
	public const string Labyrinth = "Labyrinth";
	public const string LeapController = "LeapController";
	public const string MenuController = "MenuController";
}
