using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour
{
	public GUISkin Skin;

	public Texture2D HandIcon;
	public Texture2D FistIcon;
	public Texture2D InactiveHandIcon;

	private LabController m_labController;
	private GameController m_gameController;
	private LevelsManager m_levelsMgr;
	private LeapController m_leapController;
	
	void Awake()
	{
		m_gameController = GameObject.FindGameObjectWithTag(Tags.GameController)
			.GetComponent<GameController>();

		m_levelsMgr = GameObject.FindGameObjectWithTag(Tags.LevelsManager)
			.GetComponent<LevelsManager>();

		m_leapController = GameObject.FindGameObjectWithTag(Tags.LeapController)
			.GetComponent<LeapController>();
	}

	void Update()
	{
		if(m_labController == null)
		{
			m_labController = GameObject.FindGameObjectWithTag(Tags.Labyrinth)
				.GetComponent<LabController>();
		}
	}

	void OnGUI()
	{
		GUI.skin = Skin;

		DrawGameInfo();

		if(m_labController)
			DrawHandsInfo();

		if(!m_leapController.IsLeapConnected)
			DrawNoLeap();
	}

	private void DrawGameInfo()
	{	string labyrinth;

		if(m_levelsMgr.Labyrinth != null)
			labyrinth = string.Format("Labyrinth: {0}", m_levelsMgr.Labyrinth.Name);
		else
			labyrinth = "";

		string score = string.Format("Score: {0}", m_gameController.GameStats.Score);
		string time = string.Format("Time: {0:0.00}", m_gameController.GameStats.GameTime);



		Rect area = new Rect(GUIConsts.SMALL_SPACE+20, 
		                     GUIConsts.SMALL_SPACE+20, 
		                     120, 90);

		GUI.Box(GUIUtils.MenuBox(area), "");

		GUILayout.BeginArea(area);
		GUILayout.BeginVertical();

			GUILayout.Label(labyrinth);
			GUILayout.Label(score);
			GUILayout.Label(time);

		GUILayout.EndVertical();
		GUILayout.EndArea();
	}

	private void DrawHandsInfo()
	{
		LeapHandsBundle bundle = m_labController.HandsBundle;

		int imgWidht = 32;
		int imgHeight = 40;

		int width = imgWidht * bundle.MaxHands + 10;

		Rect area = new Rect(Screen.width - width - 2*GUIConsts.SMALL_SPACE,
		                     Screen.height - imgHeight - 2*GUIConsts.SMALL_SPACE,
		                     width, imgHeight);

		if(bundle.MaxHands == 1)
		{	
			GUI.Box(GUIUtils.MenuBox(area), "");

			Rect imgRect = new Rect(area.x, area.y, imgWidht, imgHeight);

			DrawHandIcon(imgRect, bundle.Leftmost());
		}
		else if(bundle.MaxHands == 2)
		{
			GUI.Box(GUIUtils.MenuBox(area), "");

			Rect leftRect = new Rect(area.x + imgWidht, area.y, -imgWidht, imgHeight);
			DrawHandIcon(leftRect, bundle.Leftmost());

			Rect rightRect = new Rect(area.x + imgWidht + 10, area.y, imgWidht, imgHeight);
			DrawHandIcon(rightRect, bundle.Rightmost());
		}
	}

	private void DrawHandIcon(Rect rect, LeapHand h)
	{
		if(h.FistGesture.IsActive)
			GUI.DrawTexture(rect, FistIcon);
		else if(h.IsActive)
			GUI.DrawTexture(rect, HandIcon);
		else
			GUI.DrawTexture(rect, InactiveHandIcon);
	}
	
	private void DrawNoLeap()
	{
		Rect pos = new Rect(Screen.width - 170 - GUIConsts.SMALL_SPACE,
		                    Screen.height - 30 - GUIConsts.SMALL_SPACE,
		                    170,
		                    30);
		
		GUI.Box(pos, "No Leap device");
	}
}

