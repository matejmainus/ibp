using UnityEngine;
using System.Collections.Generic;

public class GUIUtils
{
	public const int MAX_LEVEL_TILES = 4;

	public static float GuiHorSlider(LeapGUI leapGui, Rect container, float val, float min, float max, params GUILayoutOption[] options)
	{
		float newVal = GUILayout.HorizontalSlider(val, min, max, options);
		
		return leapGui.LayoutSlider(container, newVal, min, max);
	}
	
	public static bool GuiButton(LeapGUI leapGui, Rect container, string title, params GUILayoutOption[] options)
	{
		return (GUILayout.Button(title, options)) || leapGui.LayoutButton(container);
	}

	public static Rect MenuBox(Rect area)
	{
		Rect box = area;
		box.x -= GUIConsts.BOX_PADDING;
		box.y -= GUIConsts.BOX_PADDING;
		box.width += 2 * GUIConsts.BOX_PADDING;
		box.height += 2 * GUIConsts.BOX_PADDING;

		return box;
	}

	public static LabyrinthModel DrawLevelsGrid(LeapGUI leapGui, Rect area)
	{
		Labyrinths labyrinths = Labyrinths.Instance();
		LabyrinthModel lab;
		
		//Draw row
		for(int i = 0 ; i < labyrinths.Count ; i++)
		{
			if(i % MAX_LEVEL_TILES == 0)
			{
				if(i != 0)
				{
					GUILayout.EndHorizontal();
					GUILayout.Space(GUIConsts.SMALL_SPACE);
				}
				
				GUILayout.BeginHorizontal();
			}
			
			lab = labyrinths[i];
			
			if(GuiButton(leapGui, area, lab.Name, GUILayout.MinWidth(70),
			             GUILayout.MaxWidth(70),
			             GUILayout.MinHeight(70)))
			{
				return lab;
			}
			
			GUILayout.Space(GUIConsts.SMALL_SPACE);
		}
		
		GUILayout.EndHorizontal();
		
		return null;
	}

	public static int LevelsHeight()
	{
		Labyrinths labyrinths = Labyrinths.Instance();
		
		return (1 + labyrinths.Count/(MAX_LEVEL_TILES+1)) * 70 + 100;
	}

	public static int LevelsWidth()
	{
		return (MAX_LEVEL_TILES * (70 + GUIConsts.SMALL_SPACE)) + 20;
	}

	public static void DrawBestStatsHeader()
	{
		GUIStyle labelStyle = GUI.skin.label;
		labelStyle.alignment = TextAnchor.UpperRight;

		//List header
		GUILayout.BeginHorizontal();

		GUILayout.Label(" ", GUILayout.Width(20));
		GUILayout.Label("Date time", labelStyle, GUILayout.Width(140));
		GUILayout.Label("Score", labelStyle, GUILayout.Width(70));
		GUILayout.Label("Game time", labelStyle, GUILayout.Width(90));
		
		GUILayout.EndHorizontal();
	}
	
	public static void DrawGameStatsRow(GameStats st, int pos, bool selected)
	{
		Color oldColor = GUI.color;
		
		if(selected)
			GUI.color = Color.red;
		
		GUILayout.BeginHorizontal();

		GUILayout.Label(string.Format("{0}", pos), GUILayout.Width(20));
		GUILayout.Label(string.Format("{0}", st.Played.ToString("dd.MM.yyyy HH:mm")), GUILayout.Width(140));
		GUILayout.Label(string.Format("{0}", st.Score), GUILayout.Width(70));
		GUILayout.Label(string.Format("{0:0.0}", st.GameTime), GUILayout.Width(90));
		
		GUILayout.EndHorizontal();
		
		GUI.color = oldColor;
	}
	
	public static bool DrawBestStats(LabyrinthModel lab, GameStats selectedStats, int items = 3)
	{
		List<GameStats> stats = GameStatsManager.Instance()
			.GetStats(lab);
	
		int max = Mathf.Min(stats.Count, items);
		GameStats st;
		bool resultShown = false;
		
		for (int i = 0 ; i < max ; i++)
		{
			st = stats[i];
			
			if(selectedStats == st)
				resultShown = true;
			
			DrawGameStatsRow(st, i+1, selectedStats == st);
		}
		
		return resultShown;
	}

}

