﻿using UnityEngine;
using System.Collections;

public class Tutorial : MonoBehaviour {

	public GUISkin Skin;

	public float TexturesAnimationTime = 1f;

	public Texture2D[] StartTexture;
	public Texture2D[] ControllTexture;
	public Texture2D[] PauseGesture;
	public Texture2D[] GameTexture;

	private enum State {Begin, Controll, Pause, Game0, Game1, End};

	private LeapController m_leapController;
	private LeapHandFollow m_handFollow;

	private State m_state = State.Begin;
	private int m_textureIndex;
	private float m_textureTime;

	private Vector3 m_startNormal = Vector3.zero;

	void Awake() {
		m_leapController = GameObject.FindGameObjectWithTag(Tags.LeapController)
			.GetComponent<LeapController>();

		m_handFollow = new LeapHandFollow(m_leapController);
	}

	void Start () {
		if(!Settings.Tutorial)
			enabled = false;
	}

	void OnGUI() {

		GUI.skin = Skin;

		switch(m_state)
		{
		case State.Begin:
			Begin ();
			break;
		case State.Controll:
			Controll();
			break;
		case State.Pause:
			Pause();
			break;
		case State.Game0:
		case State.Game1:
			Game();
			break;
		case State.End:
			enabled = false;
			Settings.Tutorial = false;
			break;
		}
	}
	
	private void Begin()
	{
		LeapHand hand = m_handFollow.Hand;
		if(hand != null)
		{
			NextState(State.Controll);
		}

		DrawTextureAndText(StartTexture,
		        "Put your hand over Leap Motion for start a game",
		        null);
	}

	private void Controll()
	{
		LeapHand hand = m_handFollow.Hand;
		if(hand == null)
			m_startNormal = Vector3.zero;
		else 
		{
			if(m_startNormal == Vector3.zero)
				m_startNormal = hand.NormalNormalized;
			else if(Vector3.Angle(m_startNormal, hand.NormalNormalized) > 35)
			{
				NextState(State.Pause);
			}
		}

		DrawTextureAndText(ControllTexture,
		        "Rotate your hand for controll a labyrinth",
		        null);
		
	}

	private void Pause()
	{
		LeapHand hand = m_handFollow.Hand;
		if((hand != null) && hand.FistGesture.IsActive)
		{
			NextState(State.Game0);
		}

		DrawTextureAndText(PauseGesture,
		                   "Move labyrinth in a stable position and make a fist gesture, for pause a game",
		                   null);
	}

	private void Game()
	{
		LeapHand hand = m_handFollow.Hand;
		if(m_state == State.Game0 && (hand == null || m_handFollow.NewHand))
			m_state = State.Game1;
		else if(m_state == State.Game1 && hand != null)
		{
			NextState(State.End);
		}

		DrawTextureAndText(GameTexture, 
			"Put all red balls to the marked hole", 
			"To finish the tutorial, move out and then back youn hand");
	}

	private void DrawTextureAndText(Texture2D[] textures, string message, string nextMessage)
	{
		m_textureTime += Time.deltaTime;

		if(m_textureTime > TexturesAnimationTime)
		{
			m_textureTime = 0;
			m_textureIndex = ++m_textureIndex % textures.Length;
		}

		Texture2D tex = textures[m_textureIndex];

		GUIStyle textureStyle = new GUIStyle(GUIStyle.none);
			textureStyle.alignment = TextAnchor.MiddleCenter;

		GUIStyle messageStyle = new GUIStyle(Skin.label);
			messageStyle.alignment = TextAnchor.MiddleCenter;

		Rect area = Area ();
		
		GUI.Box(GUIUtils.MenuBox(area), "");

		GUILayout.BeginArea(area);
		GUILayout.BeginVertical();

		GUILayout.Box(tex, textureStyle, GUILayout.MaxHeight(250), GUILayout.MinHeight(250));

		GUILayout.FlexibleSpace();

		GUILayout.Label(message, messageStyle);

		if(!string.IsNullOrEmpty(nextMessage))
		{
			GUILayout.Space(GUIConsts.SMALL_SPACE);

			GUILayout.Label(nextMessage, messageStyle);
		}

		GUILayout.FlexibleSpace();

		GUILayout.EndVertical();
		GUILayout.EndArea();
	}

	private void NextState(State newState)
	{
		m_state = newState;
		m_textureIndex = 0;
		m_textureTime = 0f;
	}

	private Rect Area()
	{
		int width = 360;
		int height = 350;
		
		return new Rect((Screen.width - width) /2, 
		                (Screen.height - height) / 2,
		                width,
		                height);
	}
}
