public class GUIConsts
{
	public const int BUTTON_MIN_HEIGHT = 50;
	public const int BUTTON_MIN_WIDTH = 90;
	
	public const int SMALL_SPACE = 20;

	public const int BOX_PADDING = 20;
}

