﻿using UnityEngine;
using System.Collections.Generic;

public class MainMenu : MonoBehaviour {

	public GUISkin MenuSkin;
	public GUISkin MainMenuSkin;
	public Texture2D Title;
	
	private LevelsManager m_levelsMgr;
	private LeapGUI m_leapGui;
	private LeapController m_leapController;

	private LabyrinthModel m_statsLab;

	private enum Section {Main, StartGame, Stats, StatsDetail, Settings}

	private Section m_section;

	public MainMenu()
	{
		m_section = Section.Main;
	}

	void Awake()
	{
		m_leapController = GameObject.FindGameObjectWithTag(Tags.LeapController).GetComponent
			<LeapController>();
		m_leapGui = GameObject.FindGameObjectWithTag(Tags.LeapController).GetComponent<LeapGUI>();
		m_levelsMgr = GameObject.FindGameObjectWithTag(Tags.LevelsManager).GetComponent<LevelsManager>();
	}
	
	void OnGUI()
	{
		m_leapGui.enabled = true;

		DrawTitle();

		switch (m_section)
		{
		case Section.Main:
			//DrawSettings();
			DrawMenu();
			break;
		case Section.Settings:
			DrawSettings();
			break;
		case Section.StatsDetail:
			DrawStatsDetail();
			break;
		case Section.Stats:
			DrawStats();
			break;
		case Section.StartGame:
			DrawLevels();
			break;
		}

		if(!m_leapController.IsLeapConnected)
			DrawNoLeap();

		DrawCredits();
	}
	
	void DrawMenu()
	{
		GUI.skin = MainMenuSkin;

		Rect area = MenuBox(150, 270);

		GUILayout.BeginArea(area);
		GUILayout.BeginVertical();

			if(GUIUtils.GuiButton(m_leapGui, area, "Tutorial", GUILayout.MinWidth(GUIConsts.BUTTON_MIN_WIDTH),
			                      GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
			{
				Tutorial();
			}

			GUILayout.FlexibleSpace();

			if(GUIUtils.GuiButton(m_leapGui, area, "Start game", GUILayout.MinWidth(GUIConsts.BUTTON_MIN_WIDTH),
				             GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
					ChangeSection(Section.StartGame);

			GUILayout.FlexibleSpace();

		   		if(GUIUtils.GuiButton(m_leapGui, area, "Settings", GUILayout.MinWidth(GUIConsts.BUTTON_MIN_WIDTH), 
				             GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
					ChangeSection(Section.Settings);
		
			GUILayout.FlexibleSpace();
			
			if(GUIUtils.GuiButton(m_leapGui, area, "Stats", GUILayout.MinWidth(GUIConsts.BUTTON_MIN_WIDTH), 
			                      GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
				ChangeSection(Section.Stats);	

			GUILayout.FlexibleSpace();
		
		   		if(GUIUtils.GuiButton(m_leapGui, area, "Quit", GUILayout.MinWidth(GUIConsts.BUTTON_MIN_WIDTH),
				             GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
					Quit();

		GUILayout.EndVertical();
		GUILayout.EndArea();
	}

	void DrawLevels()
	{
		GUI.skin = MenuSkin;

		Rect area = MenuBox(GUIUtils.LevelsWidth(), GUIUtils.LevelsHeight());

		GUI.Box(GUIUtils.MenuBox(area), "");

		GUILayout.BeginArea(area);
		GUILayout.BeginVertical();
	
			LabyrinthModel lab = GUIUtils.DrawLevelsGrid(m_leapGui, area);
			if(lab != null)
				StartGame(lab);

			GUILayout.FlexibleSpace();

			// Buttons
			GUILayout.BeginHorizontal();

				GUILayout.FlexibleSpace();

				if(GUIUtils.GuiButton(m_leapGui, area, "Back", GUILayout.MinWidth(GUIConsts.BUTTON_MIN_WIDTH), 
					             GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
						ChangeSection(Section.Main);

				GUILayout.FlexibleSpace();

			GUILayout.EndHorizontal();

		GUILayout.EndVertical();
		GUILayout.EndArea();
	}
	
	void DrawSettings()
	{
		GUI.skin = MenuSkin;

		Rect area = MenuBox(340, 100);

		GUI.Box(GUIUtils.MenuBox(area), "");

		GUILayout.BeginArea(area);
		GUILayout.BeginVertical();

			GUILayout.BeginHorizontal();

				GUILayout.Label("Sensitivity:", GUILayout.MinWidth(120));

			Settings.Sensitivity = GUIUtils.GuiHorSlider(m_leapGui, area, Settings.Sensitivity, 0.3f, 1f, GUILayout.MinWidth(220));

			GUILayout.EndHorizontal();

			GUILayout.FlexibleSpace();

			GUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();

				if(GUIUtils.GuiButton(m_leapGui, area, "Back", GUILayout.MinWidth(GUIConsts.BUTTON_MIN_WIDTH), 
				             GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
					ChangeSection(Section.Main);

				GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();

		GUILayout.EndVertical();
		GUILayout.EndArea();
	}

	void DrawStats()
	{
		GUI.skin = MenuSkin;

		Rect area = MenuBox(GUIUtils.LevelsWidth(), GUIUtils.LevelsHeight());
		
		GUILayout.BeginArea(area);
		GUILayout.BeginVertical();
		
		LabyrinthModel lab = GUIUtils.DrawLevelsGrid(m_leapGui, area);
		if(lab != null)
		{
			m_statsLab = lab;
			ChangeSection(Section.StatsDetail);
		}

		GUILayout.FlexibleSpace();
		
		// Buttons
		GUILayout.BeginHorizontal();
		
		GUILayout.FlexibleSpace();
		
		if(GUIUtils.GuiButton(m_leapGui, area, "Back", GUILayout.MinWidth(GUIConsts.BUTTON_MIN_WIDTH), 
		                      GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
			ChangeSection(Section.Main);
		
		GUILayout.FlexibleSpace();
		
		GUILayout.EndHorizontal();
		
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}

	void DrawStatsDetail()
	{
		GUI.skin = MenuSkin;

		Rect area = MenuBox(350, 250);
		
		GUI.Box(GUIUtils.MenuBox(area), "");
		
		GUILayout.BeginArea(area);
		GUILayout.BeginVertical();

		GUIUtils.DrawBestStatsHeader();
		GUIUtils.DrawBestStats(m_statsLab, null, 5);
		
		GUILayout.FlexibleSpace();

		// Buttons
		GUILayout.BeginHorizontal();
		
		GUILayout.FlexibleSpace();
		
		if(GUIUtils.GuiButton(m_leapGui, area, "Back", GUILayout.MinWidth(GUIConsts.BUTTON_MIN_WIDTH), 
		                      GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
			ChangeSection(Section.Stats);
		
		GUILayout.FlexibleSpace();
		
		GUILayout.EndHorizontal();

		GUILayout.EndVertical();
		GUILayout.EndArea();
	}

	void DrawTitle()
	{
		int width = 3*Screen.width / 8;
		int height = (int) ((Title.height / (float) Title.width) * width);

		Rect rct = new Rect(
			2*Screen.width/6 - width/2,
		    (Screen.height / 2 - 110 - height)/2, 
		    width, height);

		GUI.DrawTexture(rct, Title);
	}

	void DrawCredits()
	{
		GUI.skin = MenuSkin;

		Rect rct = new Rect(Screen.width-190, Screen.height - 50, 160, 30);

		GUI.Label(rct, "by Matěj Mainuš"); 
	}

	void DrawNoLeap()
	{
		GUI.skin = MenuSkin;

		Rect pos = new Rect(4*Screen.width/5 - 200,
		         Screen.height/2,
		         200,
		         30);

		GUI.Box(pos, "No Leap device");
	}

	Rect MenuBox(int width, int height)
	{
		return new Rect(
			2*Screen.width/6 - width/2,
			(Screen.height - height) / 2,
			width, height
			);
	}

	void ChangeSection(Section section)
	{
		//if(Event.current.type == EventType.Layout)
			m_section = section;
	}

	void StartGame(LabyrinthModel lab)
	{
		if(!m_levelsMgr.Loading)
			m_levelsMgr.LoadLevel(lab);
	}

	void Tutorial()
	{
		Settings.Tutorial = true;

		Labyrinths labs = Labyrinths.Instance();
		StartGame(labs[0]);
	}

	void Quit()
	{
		Application.Quit();
	}
}
