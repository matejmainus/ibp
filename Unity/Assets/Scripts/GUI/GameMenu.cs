﻿using UnityEngine;
using System.Collections.Generic;

public class GameMenu : MonoBehaviour {

	public GUISkin Skin;

	private enum Section {None, Paused, EndGame};

	private Section m_section;

	private GameController m_gameController;
	private LevelsManager m_levelsMgr;
	private LeapGUI m_leapGui;

	void Awake() {

		m_gameController = GameObject.FindWithTag(Tags.GameController)
			.GetComponent<GameController>();

		m_levelsMgr = GameObject.FindGameObjectWithTag(Tags.LevelsManager)
			.GetComponent<LevelsManager>();

		m_leapGui = GameObject.FindGameObjectWithTag(Tags.LeapController)
			.GetComponent<LeapGUI>();
	}
	
	void Update () {

		if(!m_levelsMgr.Loading && m_gameController.GameState == GameController.GameStateType.End)
		{
			m_section = Section.EndGame;
		}
		else if (Input.GetKeyDown("escape"))
		{
			m_section = m_section == Section.Paused ? Section.None : Section.Paused;
		}

		m_leapGui.enabled = m_section != Section.None;
		Time.timeScale = m_section == Section.None ? 1f : 0f;
	}

	void OnGUI()
	{
		GUI.depth = 1;

		GUI.skin = Skin;
		
		switch (m_section)
		{
		case Section.Paused:
			DrawPauseMenu();
			break;
		case Section.EndGame:
			DrawEndGameMenu();
			break;
		}
	}

	void DrawPauseMenu()
	{
		Rect area = new Rect(
			(Screen.width-450) / 2,
			4*Screen.height/5 - 55,
			450, 55
			);

		GUI.Box(GUIUtils.MenuBox(area), "");

		GUILayout.BeginArea(area);
		GUILayout.BeginHorizontal();

			if(GUIUtils.GuiButton(m_leapGui, area, "Resume", GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
				m_section = Section.None;

			GUILayout.Space(GUIConsts.SMALL_SPACE);

			if(GUIUtils.GuiButton(m_leapGui, area, "Main menu", GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
				LoadMainMenu(true);

			GUILayout.Space(GUIConsts.SMALL_SPACE);

			if(GUIUtils.GuiButton(m_leapGui, area, "Restart level", GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
				RestartLevel();

			GUILayout.Space(GUIConsts.SMALL_SPACE);
			
			if(GUIUtils.GuiButton(m_leapGui, area, "Quit app",  GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
				QuitGame();

		GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}

	void DrawEndGameMenu()
	{
		Rect area = new Rect(
			(Screen.width-360) / 2,
			(Screen.height - 240) / 2,
			360, 240
			);

		GUI.Box(GUIUtils.MenuBox(area), "");

		GUILayout.BeginArea(area);
		GUILayout.BeginVertical();

			GUIUtils.DrawBestStatsHeader();

			bool resultShown = GUIUtils.DrawBestStats(m_levelsMgr.Labyrinth, m_gameController.GameStats);

			if(!resultShown)
			{
				GUILayout.Space(GUIConsts.SMALL_SPACE);

			 	GameStatsManager statsMgr = GameStatsManager.Instance();
				int pos = statsMgr.GetStats(m_levelsMgr.Labyrinth).IndexOf(m_gameController.GameStats)+1;

				GUIUtils.DrawGameStatsRow(m_gameController.GameStats, pos, true);
			}

			GUILayout.FlexibleSpace();

			//Buttons
			GUILayout.BeginHorizontal();
		
				GUILayout.FlexibleSpace();

				if(GUIUtils.GuiButton(m_leapGui, area, "Main menu", GUILayout.MinWidth(90), 
				             GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
					LoadMainMenu(false);

				GUILayout.Space(GUIConsts.SMALL_SPACE);
		
				if(GUIUtils.GuiButton(m_leapGui, area, "Try again", GUILayout.MinWidth(90), 
				             GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
					RestartLevel();

				if(HasNext())
				{
					GUILayout.Space(GUIConsts.SMALL_SPACE);

					if(GUIUtils.GuiButton(m_leapGui, area, "Next level", GUILayout.MinWidth(90), 
					                      GUILayout.MinHeight(GUIConsts.BUTTON_MIN_HEIGHT)))
						LoadNext();
				}

				GUILayout.FlexibleSpace();
		
			GUILayout.EndHorizontal();

		GUILayout.EndVertical();
		GUILayout.EndArea();
	}

	private void QuitGame()
	{
		m_gameController.AbortGame();

		Application.Quit();
	}

	private void LoadMainMenu(bool abort)
	{
		m_section = Section.None;

		if(abort)
			m_gameController.AbortGame();

		if(!m_levelsMgr.Loading)
			m_levelsMgr.LoadMainMenu();
	}

	private void RestartLevel()
	{
		m_section = Section.None;
		m_gameController.AbortGame();

		if(!m_levelsMgr.Loading)
			m_levelsMgr.RestartLevel();
	}

	private void LoadNext()
	{
		m_section = Section.None;

		LabyrinthModel next = Labyrinths.Instance().Next(m_levelsMgr.Labyrinth);

		if(next != null)
			m_levelsMgr.LoadLevel(next);
	}

	private bool HasNext()
	{
		return Labyrinths.Instance().Next(m_levelsMgr.Labyrinth) != null;
	}
}
