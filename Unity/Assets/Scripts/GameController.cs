﻿using UnityEngine;
using System.Collections.Generic;
using Leap;

public class GameController : MonoBehaviour {

	const int BALL_IN_HELL = -2;
	const int BALL_IN_HEAVEN = 5;
	
	public enum GameStateType {Start = 0,Ready,Play,Pause,End,Aborted};
	
	public GameStateType GameState {get; private set;}
	public GameStats GameStats {get; private set;}

	private LevelsManager m_levelsMgr;

	public GameController()
	{
		GameState = GameStateType.Ready;
		GameStats = new GameStats();
	}

	void Awake ()
	{
		m_levelsMgr = GameObject.FindGameObjectWithTag(Tags.LevelsManager)
			.GetComponent<LevelsManager>();
	}

	void Update () 
	{
		if(GameState == GameStateType.Play)
			GameStats.GameTime += Time.deltaTime;
	}

	public void StartGame()
	{
		GameStats = new GameStats();
		GameState = GameStateType.Start;
	}

	public void ReadyGame()
	{
		GameState = GameStateType.Ready;
	}

	public void PauseGame()
	{
		GameState = GameStateType.Pause;
	}

	public void ResumeGame()
	{
		GameState = GameStateType.Play;
	}
	
	public void EndGame()
	{
		GameState = GameStateType.End;

		SaveGameStats();
	}

	public void AbortGame()
	{
		GameState = GameStateType.Aborted;
	}

	public void BallInHell()
	{
		GameStats.Score += BALL_IN_HELL;
	}

	public void BallInHeaven()
	{
		GameStats.Score += BALL_IN_HEAVEN;
	}

	public void SaveGameStats()
	{
		GameStatsManager statsMgr = GameStatsManager.Instance();
		
		statsMgr.AddStats(m_levelsMgr.Labyrinth, GameStats);
		statsMgr.Save();
	}
}
