﻿
public class LabyrinthModel {

	public int Id {get; private set;}
	public string Name {get; private set;}
	public string ResourceName {get; private set;}

	public int BallCount {get; private set;}
	public int ActiveBallCount {get; private set;}
	
	public LabyrinthModel(int id, string name, string resourceName,
	                      int ballCount, int activeBallCount)
	{
		Id = id;
		Name = name;
		ResourceName = resourceName;
		BallCount = ballCount;
		ActiveBallCount = activeBallCount;
	}
}
