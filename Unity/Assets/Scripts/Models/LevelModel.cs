﻿
public class LevelModel {

	public int Id {get; private set;}
	public string LevelName {get; private set;}

	public LevelModel(int id, string levelName)
	{
		Id = id;
		LevelName = levelName;
	}
}
