﻿using System.Collections.Generic;

public class Labyrinths : List<LabyrinthModel> {

	private static Labyrinths m_instance = new Labyrinths();
	
	public static Labyrinths Instance()
	{
		return m_instance;
	}

	public LabyrinthModel Next(LabyrinthModel current)
	{
		int index = IndexOf(current);

		if(index >= 0 && index < this.Count-2)
			return this[index+1];
		else
			return null;
	}

	private Labyrinths()
	{
		Add(new LabyrinthModel(0, "1-1", "01/Lab_prefab", 2, 1));
		Add(new LabyrinthModel(1, "1-2", "01/Lab_prefab", 4, 2));
		Add(new LabyrinthModel(2, "2-1", "02/Lab_prefab", 1, 1));
		Add(new LabyrinthModel(3, "2-2", "02/Lab_prefab", 2, 1));
		Add(new LabyrinthModel(4, "2-3", "02/Lab_prefab", 3, 2));
		Add(new LabyrinthModel(5, "3-1", "03/Lab_prefab", 1, 1));
		Add(new LabyrinthModel(6, "3-2", "03/Lab_prefab", 2, 1));
		Add(new LabyrinthModel(7, "3-3", "03/Lab_prefab", 3, 2));
	}
}
