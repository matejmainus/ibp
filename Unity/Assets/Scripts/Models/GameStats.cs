﻿using System;
using UnityEngine;

[Serializable]
public class GameStats : IComparable<GameStats>{

	public DateTime Played {get; private set;}

	public int Score {get; set;}
	public float GameTime {get; set;}

	public GameStats()
	{
		Played = DateTime.UtcNow;

		Score = 0;
		GameTime = 0f;
	}

	public int CompareTo (GameStats other)
	{
		int scoreDiff = -Score.CompareTo(other.Score);

		if(scoreDiff != 0)
			return scoreDiff;
		else
			return GameTime.CompareTo(other.GameTime);
	}
}
