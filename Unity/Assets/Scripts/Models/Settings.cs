using UnityEngine;

public class Settings
{
	private static string KEY_SENSITIVITY = "Sensitivity";
	private static string KEY_TUTORIAL = "Tutorial";

	public static float Sensitivity {
		get {
			return PlayerPrefs.GetFloat(Settings.KEY_SENSITIVITY, 0.4f);
		}
		set {
			PlayerPrefs.SetFloat(Settings.KEY_SENSITIVITY, value);
		}
	}

	public static bool Tutorial {
		get {
			return PlayerPrefs.GetInt(KEY_TUTORIAL) == 0;
		}
		set {
			PlayerPrefs.SetInt(KEY_TUTORIAL, value ? 0 : 1);
		}
	}
}

