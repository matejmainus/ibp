﻿using System.Collections.Generic;

public class Levels : List<LevelModel>{
	
	private static Levels m_instance = new Levels();

	public static Levels Instance()
	{
		return m_instance;
	}

	private Levels()
	{
		Add(new LevelModel(0, "Scene1"));
		Add(new LevelModel(1, "Scene2"));
	}
}
